package com.slidingimages.networking;

import com.slidingimages.model.Example;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;


/**
 * Created by Aleesha Kanwal
 */

public interface JsonApiInterface {

    @GET("apis/news/api/getNews")
    Call<Example> getNews();

}
