package com.slidingimages;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.slidingimages.model.News;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class SlidingImage_Adapter extends PagerAdapter {


    private ArrayList<String> IMAGES;
    List<News> newsArrayList;
    private LayoutInflater inflater;
    private Context context;


    public SlidingImage_Adapter(Context context,ArrayList<String> IMAGES,List<News> newsArrayList) {
        this.context = context;
        this.IMAGES=IMAGES;
        this.newsArrayList = newsArrayList;
        inflater = LayoutInflater.from(context);



    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slidingimages_layout, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.image);
        final TextView textView = (TextView) imageLayout.findViewById(R.id.desc);
        final TextView title = (TextView) imageLayout.findViewById(R.id.title);
        final TextView date = (TextView) imageLayout.findViewById(R.id.date);
        textView.setMovementMethod(new ScrollingMovementMethod());
        Picasso.with(context).load(IMAGES.get(position)).into(imageView);
        textView.setText(newsArrayList.get(position).getPostDescription());
        title.setText(newsArrayList.get(position).getPostTitle());
        date.setText(newsArrayList.get(position).getCreatedOn());


      //  imageView.setImageResource(IMAGES.get(position));

        view.addView(imageLayout, 0);


        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}
